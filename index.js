// console.log("Hello World");

/*
8. Create a constructor for creating a pokemon with the following properties:
Name (Provided as an argument to the constructor)
Level (Provided as an argument to the constructor)
Health (Create an equation that uses the level property)
Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon objects from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
*/

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
        hoenn:["May","Max"],
        kanto:["Brock", "Misty"]
    },
    talk: function(){
    	console.log("Pikachu! I choose you!");
    }
}
console.log(trainer);
console.log("Result of dot notation: ")
console.log(trainer.name);
console.log("Result of square bracket notation: ")
console.log(trainer.pokemon);

console.log("Result of talk method");
trainer.talk();

//

function Pokemon(name, level,){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
	this.tackle = function(target){
		target.healthRemain=target.health-this.attack;
		target.health=target.healthRemain;
		console.log(this.name +" tackled "+ target.name);
		console.log(target.name+"'s' health is now reduced to "+target.healthRemain);
		if(target.healthRemain<=0){
		target.faint();
	}
	this.faint = function (){
		console.log(this.name + " fainted.");
	}
}
}
let pikachu = new Pokemon("Pikachu", 12)
let geodude = new Pokemon("Geodude", 8)
let mewtwo = new Pokemon("mewtwo", 100)
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);
geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);
